let bodyParser = require('body-parser'),
    fs = require('fs'),
    urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = (app) => {
    app.post('/pair', urlencodedParser, function (req, res) {
        if (!req.body.pair) res.send(404); // Return 404 error if no 'pair' parameter in request body
        let pair = req.body.pair.split(','), // Split 'pair' parameter by ','
            // Callback function
            answer = (json) => {
                res.json(json);
            },
            file = fs.createReadStream(__dirname + '/../docs/summaries.json', { encoding: 'utf8', highWaterMark: 128 * 1024 });
        // Read cached file content
        file.on('data', function (body) {
            const regex = /(.+):=?(.+)/; // Regexp to work with 'cryptowatch' API
            let allMarkets = JSON.parse(body).result, // Parse all markets from 'cryptowatch' and save it
                marketsCollection = {}, // Response body
                // Function which add props to response object
                markets = (exchange, body) => {
                    let tmp = {
                        exchange: exchange,
                    };
                    return Object.assign(tmp, body); // Merge 'tmp' and request body
                };
            // Looping all markets
            for (let i in allMarkets) {
                if (allMarkets.hasOwnProperty(i)) {
                    let m;
                    // Check if regexp match and not null
                    if ((m = regex.exec(i)) !== null) {
                        for (let n in pair) {
                            if (pair.hasOwnProperty(n)) {
                                // Check if market base equal to request 'pair' parameter
                                if (pair[n] === m[2]) {
                                    // Check if object already have 'pair'
                                    if (!marketsCollection[m[2]]) marketsCollection[m[2]] = [];
                                    // Delete unusable parameter from request
                                    delete allMarkets[i].volume;
                                    // Push markets to one object
                                    marketsCollection[m[2]].push(markets(m[1], allMarkets[i]))
                                }
                            }
                        }
                    }
                }
            }
            answer(marketsCollection); // Execute callback function
        })
    });
    app.get('/summaries', function (req, res) {
        let file = fs.createReadStream(__dirname + '/../docs/summaries.json', { encoding: 'utf8', highWaterMark: 128 * 1024 });
        file.on('data', function (body) {
            res.send(JSON.parse(body));
        });
    });
    app.get('/pairs', function (req, res) {
        let file = fs.createReadStream(__dirname + '/../docs/pairs.json', { encoding: 'utf8', highWaterMark: 128 * 1024 });
        file.on('data', function (body) {
            res.send(JSON.parse(body));
        });
    })
};