let express = require('express'),
    app = express(),
    apiController = require('./controllers/apiController');

apiController(app);

app.set('port', (process.env.PORT || 5000));

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});