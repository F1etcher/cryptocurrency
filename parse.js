let request = require('request'),
    fs = require('fs');

setInterval(function(){
    request({
        url: "https://api.cryptowat.ch/markets/summaries",
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
    },function(error, response, body){
        if(!error && response.statusCode === 200){
            let file = fs.createWriteStream(__dirname + '/docs/summaries.json');
            file.write(body);
            console.log(JSON.parse(body).allowance);
        }else{
            console.log('error' + response.statusCode);
        }
    });
}, 2 * 1000);

setInterval(function(){
    request({
        url: "https://api.cryptowat.ch/pairs",
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
    },function(error, response, body){
        if(!error && response.statusCode === 200){
            let file = fs.createWriteStream(__dirname + '/docs/pairs.json');
            file.write(body);
            console.log(JSON.parse(body).allowance);
        }else{
            console.log('error' + response.statusCode);
        }
    });
}, 1 * 1000);